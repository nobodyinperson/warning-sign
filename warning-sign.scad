// clang-format off
include <Round-Anything/polyround.scad>;
// clang-format on

/* [Sign] */
sign_side_length = 50; // [10:0.1:300]
sign_edge_radius = 1;
sign_thickness = 2;
text_offset_y = 2;

/* [Precision] */
$fs = $preview ? 1 : 0.5;
$fa = $preview ? 1 : 0.5;
epsilon = 0.1;

function equilateralTriangleInnerRadius(sideLength) = sideLength * sqrt(3) / 3;
function equilateralTriangleHeight(sideLength) = sideLength * sqrt(3) / 2;

sign_inner_radius = equilateralTriangleInnerRadius(sign_side_length);
sign_height = equilateralTriangleHeight(sign_side_length);

signShapePoints = [
  [ -sign_side_length / 2, 0, sign_edge_radius ],
  [ sign_side_length / 2, 0, sign_edge_radius ],
  [ 0, sign_height, sign_edge_radius ],
];

signShapePointsCentered =
  translateRadiiPoints((signShapePoints),
                       [ 0, -sign_height + sign_inner_radius ]);

color("red")
{
  difference()
  {
    linear_extrude(sign_thickness)
      polygon(polyRound(signShapePointsCentered, fn = 30));

    translate([ 0, text_offset_y, sign_thickness / 2 + epsilon ])
      linear_extrude(sign_thickness * 2 + epsilon) text(
        "!", size = sign_inner_radius, halign = "center", valign = "center");
  }
}

